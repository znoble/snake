import sys, pygame,math, random as rand
from pygame.colordict import THECOLORS as cd

pygame.init()
if not pygame.font: print("Warning, fonts disabled")

size = width, height = 400, 400
black = 0, 0, 0
screen = pygame.display.set_mode(size)
clock = pygame.time.Clock()

s = False
while not s:
    clock.tick(60)
    for event in pygame.event.get():
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_SPACE:
                s = True
    if pygame.font:
        font = pygame.font.Font(None, 36)
        text = font.render("Press Space to Start.", 1, (255,255,255))
        textpos = text.get_rect(centerx=width//2, centery=height//2)
        screen.blit(text, textpos)
    pygame.display.flip()


direction = [0,-1]
body = [pygame.Rect(width//2, height//2, 10, 10)]
pbody = [[0,0]]
updatetimer = 0
updatetimer_cap = 20

food = pygame.Rect(0, 0, 10, 10)
food.left, food.top = rand.randint(10, width//10) * 10 - food.width, rand.randint(10, height//10) * 10 - food.height

gameover = False

while not gameover:
    clock.tick(60)
    
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            print("GAME OVER.\n Score: {0}".format(len(body[1:])))
            sys.exit()
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_w and direction != [0, 1]:
                direction = [0,-1]
            if event.key == pygame.K_s and direction != [0, -1]:
                direction = [0, 1]
            if event.key == pygame.K_a and direction != [1, 0]:
                direction = [-1,0]
            if event.key == pygame.K_d and direction != [-1, 0]:
                direction = [1, 0]

    
#Update            
    if updatetimer >= updatetimer_cap:
        
#save prev body position
        for i in range(len(body)):
            pbody[i] = [body[i].left, body[i].top]
        
#update current body position
        body[0].left += direction[0] * body[0].width
        body[0].top  += direction[1] * body[0].height
        for i in range(1, len(body)):
            body[i].left = pbody[i-1][0]
            body[i].top  = pbody[i-1][1]        

        updatetimer = 0
    updatetimer += 1


#Collision with food
    if food.colliderect(body[0]):
        food.left, food.top = rand.randint(10, width//10) * 10 - food.width, rand.randint(10, height//10) * 10 - food.height
        if updatetimer_cap >= 5:
            updatetimer_cap -= 0.5

    #add new tail seg
        tail = pygame.Rect(
            body[len(body) - 1].left - direction[0] * 10,
            body[len(body) - 1].top -  direction[1] * 10,
            10, 10)
        body.append(tail)
        pbody.append([tail.left, tail.top])

#Collision with tail
    for i in range(1, len(body)):
        if body[i].colliderect(body[0]):
            gameover = True
            #pygame.event.post(pygame.event.Event(pygame.QUIT))

#Collision with wall
    if body[0].top < 0 or body[0].bottom > height or body[0].left < 0 or body[0].right > width:
        gameover = True
        #pygame.event.post(pygame.event.Event(pygame.QUIT))

#Draw Snake and Food
    screen.fill(black)
    for seg in body:
        pygame.draw.rect(screen, cd['green'], seg)
    pygame.draw.rect(screen, cd['red'], food)

#Draw Hud
    if pygame.font:
        font = pygame.font.Font(None, 24)
        text = font.render("Score {0}".format(len(body[1:])), 1, (255, 255, 255))
        textpos = text.get_rect(centerx=width//2)
        screen.blit(text, textpos)

    pygame.display.flip()

#Game over screen
e = False
while not e:
    clock.tick(60)
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            pygame.event.post(pygame.event.QUIT)
            sys.exit()
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_SPACE:
                e = True
    if pygame.font:
        font = pygame.font.Font(None, 28)
        text = font.render("Game Over. Press Space To Exit", 1, (255, 255, 255))
        textpos = text.get_rect(centerx=width//2, centery=height//2)
        screen.blit(text, textpos)
    pygame.display.flip()

